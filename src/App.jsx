import { useEffect, useState } from 'react';
import Pagination from './components/Pagination';

function App({ limit }) {
  const [loading, setLoading] = useState(true);
  const [users, setUsers] = useState([]);
  const [total, setTotal] = useState(0);
  const [skip, setSkip] = useState(0);

  useEffect(() => {
    setLoading(true);
    fetch(`https://dummyjson.com/users?skip=${skip}&limit=${limit}`)
      .then((res) => res.json())
      .then((data) => {
        setUsers(data.users);
        setTotal(data.total);
      })
      .finally(() => {
        setLoading(false);
      });
  }, [skip]);

  function handlePageChange(page) {
    const nextSkip = (page - 1) * LIMIT;
    setSkip(nextSkip);
  }

  return (
    <>
      <ul data-testid="user-list">
        {users.map((user) => (
          <li key={user.id}>
            <strong>
              {user.firstName} {user.lastName} (@{user.username})
            </strong>
          </li>
        ))}
      </ul>
      {loading && <strong>Loading...</strong>}
      <Pagination
        nextLabel="Növbəti"
        prevLabel="Əvvəlki"
        onPageChange={handlePageChange}
        total={total}
      />
    </>
  );
}

export default App;
