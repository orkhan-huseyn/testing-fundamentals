import {
  render,
  screen,
  cleanup,
  waitForElementToBeRemoved,
} from '@testing-library/react';
import { it, expect, beforeEach, vi } from 'vitest';
import App from './App';

const fakeData = {
  users: [
    {
      id: 1,
      firstName: 'Test',
      lastName: 'Test',
      username: 'test',
    },
  ],
  total: 10,
};

const mockFetch = vi.fn(() => {
  return Promise.resolve({
    ok: true,
    json() {
      return Promise.resolve(fakeData);
    },
  });
});

vi.stubGlobal('fetch', mockFetch);

beforeEach(() => {
  cleanup();
});

it('calls fetch method with correct parameters', () => {
  render(<App limit={10} />);
  expect(mockFetch).toHaveBeenCalledOnce();
  expect(mockFetch).toHaveBeenCalledWith(
    'https://dummyjson.com/users?skip=0&limit=10'
  );
});

it('shows loading when the first rendered', () => {
  render(<App limit={10} />);
  expect(screen.getByText('Loading...')).toBeDefined();
});

it('the user list after the response is returned from API', async () => {
  render(<App limit={10} />);
  await waitForElementToBeRemoved(screen.getByText('Loading...'));
  expect(screen.getByTestId('user-list').children).toHaveLength(1);
});
