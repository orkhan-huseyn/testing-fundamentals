import { render, screen, cleanup } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { describe, it, expect, beforeEach, vi } from 'vitest';
import Pagination from './Pagination';

beforeEach(() => {
  cleanup();
});

describe('Pagination', () => {
  it('renders list element which prev and next buttons when passed no props', () => {
    render(<Pagination />);
    const nav = screen.getByRole('list');
    const prevButton = screen.getByText('<<');
    const nextButton = screen.getByText('>>');

    expect(nav).not.toBeNull();
    expect(prevButton).toBeDefined();
    expect(nextButton).toBeDefined();
  });

  it('renders list element which prev, next 10 page buttons when total is 100', () => {
    render(<Pagination total={100} />);
    const nav = screen.getByRole('list');
    expect(nav.children).toHaveLength(12);
  });
});

it('highlights first page when rendered with no default current page prop', () => {
  render(<Pagination total={100} />);
  const pageOneContainer = screen.getByTestId('page-1-container');
  expect(pageOneContainer.className).toContain('active');
});

it('highlights correct page when that page number is clicked', async () => {
  render(<Pagination total={100} />);
  const pageFiveBtn = screen.getByText('5');
  await userEvent.click(pageFiveBtn);

  const pageOneContainer = screen.getByTestId('page-5-container');
  expect(pageOneContainer.className).toContain('active');
});

it('call onPageChange handler function with correct arguments when page button is clicked', async () => {
  const handlePageChange = vi.fn();
  render(<Pagination onPageChange={handlePageChange} total={100} />);
  const pageFiveBtn = screen.getByText('5');
  await userEvent.click(pageFiveBtn);
  expect(handlePageChange).toHaveBeenCalledOnce();
  expect(handlePageChange).toHaveBeenCalledWith(5);
});
