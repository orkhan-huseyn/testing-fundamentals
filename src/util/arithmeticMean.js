export function arithmeticMean(numbers = []) {
  if (numbers.length == 0) return 0;
  let sum = 0;
  for (const number of numbers) sum += number;
  return sum / numbers.length;
}
