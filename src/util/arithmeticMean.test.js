import { it, expect } from 'vitest';
import { arithmeticMean } from './arithmeticMean';

it('returns zero when empty array is passed', () => {
  let numbers = [];
  let expectedResult = 0;

  let result = arithmeticMean(numbers);

  expect(result).toBe(expectedResult);
});

it('returns zero when no arguments are passed', () => {
  let numbers = undefined;
  let expectedResult = 0;

  let result = arithmeticMean(numbers);

  expect(result).toBe(expectedResult);
});

it('returns first number when all of numbers are the same', () => {
  let numbers = [3, 3, 3, 3, 3];
  let expectedResult = 3;

  let result = arithmeticMean(numbers);

  expect(result).toBe(expectedResult);
});

it('returns correct decimal when all of different numbers are passed', () => {
  let numbers = [1, 5, 6, 10, 4];
  let expectedResult = 5.2;

  let result = arithmeticMean(numbers);

  expect(result).toBe(expectedResult);
});
